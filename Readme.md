# Jobtome redis go app
## What is this?
This is a simple application with a CI workflow powered by Gitlab CI. It also includes kubernetes manifest, but the CD
deployment must be handled by the cluster, using tools like flux or ArgoCD.

## How it works
You just need to merge a change to master and let the pipeline do the work. A docker image will be generated and pushed
to GitLab Registry if everything goes well with the testing.

## What is missing
Due to the time constraint a lot of thing can be improved (I mean a lot).
 - The service is exposed by as LoadBalancer. In a real world cluster, it would have an ingress, probably behind an
nginx ingress controller.
 - In order to complete the workflow, we would need a CD tool as mention before. Flux could be an option.
 - Of course, you need a kubernetes cluster
 - The application needs a redis server to work properly. You can consider it as part of the app and deploy inside the
same pod, deploy it with helm or other many options.
- In a real application, you would probably run it in you local machine, a docker-compose would be one of the best 
  options, including the app and redis.