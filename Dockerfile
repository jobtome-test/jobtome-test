FROM golang:1.16-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /redis-go-app

# CGO has to be disabled for alpine

FROM alpine:3.14

ENV REDIS_URL=redis-svc:6379
ENV SERVICE_ADDR=:3000

WORKDIR /

# Add CGO libraries
RUN apk add --no-cache libc6-compat

COPY --from=build /redis-go-app /redis-go-app

EXPOSE 3000

ENTRYPOINT ["/redis-go-app"]